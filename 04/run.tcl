set search_path "/cad/CBDK/CBDK_TSMC90GUTM_Arm_v1.2/CIC/SynopsysDC/db $search_path"

set target_library    "typical.db"
set link_library      "$target_library"

read_file -format verilog -netlist ../02/mult16p3_gate.v
current_design mult16p3
link
set_wire_load_model -name tsmc090_wl10 -library typical

report_area > ./Report/Area.txt
report_timing > ./Report/Timing.txt

set test_default_scan_style multiplexed_flip_flop

report_constraint -all_violators > ./Report/Constraint.txt
create_test_protocol -infer_clock -infer_async
dft_drc
set_scan_configuration -chain_count 1
preview_dft > ./Report/Dft.txt

insert_dft

report_constraint -all_violators > ./Report/Constraint2.txt
dft_drc
dft_drc -coverage_estimate

report_scan_path -view existing -chain all > ./Report/ScanPath.txt

report_area > ./Report/Area2.txt
report_timing > ./Report/Timing2.txt

write -format verilog -hierarchy -output mult16p3_gate_scan.v

exit