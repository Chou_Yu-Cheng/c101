set search_path "/cad/CBDK/CBDK_TSMC90GUTM_Arm_v1.2/CIC/SynopsysDC/db $search_path"
define_design_lib WORK -path ./work

set target_library    "typical.db"
set link_library      "$target_library dw_foundation.sldb"
set symbol_library    "generic.sdb"
set synthetic_library "dw_foundation.sldb"

analyze -format verilog -library WORK [list ../01/mult16p3.v]
elaborate mult16p3 -arch verilog -library WORK
current_design mult16p3
set_wire_load_model -name tsmc090_wl10 -library typical

compile -ungroup_all

report_area > ./Report/Area.txt
report_timing -path full -delay max -max_path 1 -nworst 1 > ./Report/Timing.txt
report_reference > ./Report/Reference.txt

create_clock -name clk -period 5 -waveform [list 0 2.5] [list clk]
set_clock_uncertainty -setup 0.5 clk
set_clock_uncertainty -hold 0.5 clk
set_output_delay 0.5 -clock clk [all_outputs]
set_dont_touch_network [find clock clk]
set_driving_cell -lib_cell INVX1 [all_inputs]
set drive 0 [list clk]
set fix hold [find clock clk]

report_clock > ./Report/Clock.txt

set_load 0.075 [all_outputs]
set_max_transition 0.5 [find design mult16p3]
set_fix_multiple_port_nets -feedthroughs

compile -map_effort high

report_area > ./Report/Area2.txt
report_timing -path full -delay max -max_path 1 -nworst 1 > ./Report/Timing2.txt
report_reference > ./Report/Reference2.txt

change_names -rule verilog -verbose -hierarchy

set verilogout_no_tri TRUE
set verilogout_single_bit FALSE
set verilogout_show_unconnected_pins TRUE

write -format verilog -hierarchy -output mult16p3_gate.v

exit