set search_path ". /cad/CBDK/CBDK_TSMC90GUTM_Arm_v1.2/CIC/SynopsysDC/db $search_path"

set link_library "* typical.db"

read_verilog ../02/mult16p3_gate.v
link_design mult16p3

set_driving_cell -lib_cell INVX1 [all_inputs]
set_load -pin_load 0.075 [all_outputs]
set_wire_load_model -name tsmc090_wl10 -library typical

check_timing > ./Report/CTiming.txt

create_clock -period 5 -name CLK [get_ports clk]
set_output_delay 0.5 -clock CLK [all_outputs]

check_timing > ./Report/CTiming2.txt
report_timing > ./Report/Timing.txt
report_delay_calculation -from U1111/A -to U1111/Y > ./Report/CellDelay.txt

write_sdf -no_edge mult16p3.sdf

quit